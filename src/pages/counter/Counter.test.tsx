import { shallow } from 'enzyme';
import Counter from './Counter';
import { render, screen } from '@testing-library/react';
import { useSelector } from 'react-redux';

jest.mock('react-redux', () => {
  const actualModule = jest.requireActual('react-redux');
  return {
    ...actualModule,
    useDispatch: () => {
      const mockFn = jest.fn();
      return mockFn;
    },
    useSelector: jest.fn(),
  };
});

const mockUseSelector = useSelector as jest.Mock;

describe('Counter component', () => {
  // test using Enzyme
  it('should render properly', () => {
    mockUseSelector.mockImplementationOnce(() => 0); // mock counter value
    const wrapper = shallow(<Counter />);
    expect(wrapper).toMatchSnapshot();
  });

  // test using React Testing Library
  it('should display the correct count value', () => {
    mockUseSelector.mockImplementationOnce(() => 0); // mock counter value
    render(<Counter />);
    expect(screen.getByText('You clicked 0 times')).toBeInTheDocument();
  });
});
