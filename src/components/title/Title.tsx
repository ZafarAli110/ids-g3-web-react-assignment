export type Props = {
  text: string;
  dataTestId?: string;
};

export default function Title({ text, dataTestId }: Props) {
  return (
    <h3 title={text} data-testid={dataTestId || ''}>
      <u>{text}</u>
    </h3>
  );
}
