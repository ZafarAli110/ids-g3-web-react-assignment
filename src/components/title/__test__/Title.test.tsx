import { render, screen } from '@testing-library/react';
import Title from '../Title';

describe('Title component', () => {
  // getByText -> looks for a specific text and returns the element
  // it will throw an error if it cannot find the text or if there are
  // more than one element with that text
  it('should render same text passed as a prop', () => {
    const { getByText } = render(<Title text="Hello World" />);
    expect(getByText('Hello World')).toBeInTheDocument();
  });

  // getByRole -> looks for a specific role and returns the element
  // it will throw an error if it cannot find the role or there are
  // more than one element with that role.
  // NOTE: 'test' and 'it' are pretty much the same thing, for the purpose
  // of consistency, use one or the other and not both
  test('should render same text passed as a prop', () => {
    render(<Title text="Title Component" />);
    expect(screen.getByRole('heading')).toHaveTextContent('Title Component');
    // you can also pass regex
    expect(screen.getByText(/title component/i)).toBeInTheDocument();
  });

  // this test wont pass if we have more than one heading element
  // in the document, try to put more than one heading element
  // in the Title component and see if it fails
  it('should render same text passed as a prop', () => {
    render(<Title text="Hello" />);
    const headingElement = screen.getByRole('heading');
    expect(headingElement).toBeInTheDocument();
  });

  // this test wont fail if we have more than one heading element
  // in the document, since we are looking for a specific heading element using
  // the name, try to put more than one heading element in the Title component
  // and see if it passes
  it('should render same text passed as a prop', () => {
    render(<Title text="Title Component" />);
    const headingElement = screen.getByRole('heading', {
      name: 'Title Component',
    });
    expect(headingElement).toBeInTheDocument();
  });

  // asserting that the heading element should be the h3 element
  it('should render the h3 element', () => {
    const { getByRole } = render(<Title text="Hello World" />);
    const headingElement = getByRole('heading');
    expect(headingElement).toContainHTML('h3');
  });

  // asserting the textContent of the heading element
  it('should render the h3 element', () => {
    const { getByRole } = render(<Title text="Hello World" />);
    const headingElement = getByRole('heading');
    expect(headingElement.textContent).toBe('Hello World');
  });

  // getByTitle -> looks for an element with a title attribute
  // eg) <button title="Increment">Increment</button> here the value of
  // title attribute is "Increment"
  it('should render same text passed as a prop', () => {
    render(<Title text="Title Component" />);
    const headingElement = screen.getByTitle(/Title component/i); // using regex
    expect(headingElement).toBeInTheDocument();
  });

  // getByTestId -> looks for an element with a data-testid attribute
  // eg) <button data-test-id="Increment">Increment</button> here the value of
  // data-testid attribute is "Increment"
  // NOTE: should not use camelCase for data-testid attribute nor should
  // use hyphen for data-testid attribute
  // eg) data-test-id="Increment" or data-testId="increment"
  it('should render same text passed as a prop', () => {
    render(<Title text="Hello" dataTestId="Title-header" />);
    const headingElement = screen.getByTestId('Title-header');
    expect(headingElement).toBeInTheDocument();
  });

  // findByText -> looks for an element with a text content
  // IMPORTANT: All findBy queries are async, so we need to use async/await
  it('should render same text passed as a prop', async () => {
    render(<Title text="Title Component" />);
    const headingElement = await screen.findByText(/Title component/i);
    expect(headingElement).toBeInTheDocument();
  });

  // queryBy --> looks for an element and return null if not found
  // NOTE: it wont throw an error if the element is not found
  it('should render same text passed as a prop', () => {
    render(<Title text="Title Component" />);
    const headingElement = screen.queryByText(/Apple/i);
    expect(headingElement).not.toBeInTheDocument();
  });

  // getAllByRole --> looks for an element returns an array of elements
  // Note: All api with 'All' in the name returns an array of elements
  it('should render same text passed as a prop', () => {
    render(<Title text="Title Component" />);
    const headingElements = screen.getAllByRole('heading');
    expect(headingElements.length).toBe(1);
  });
});
